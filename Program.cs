﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp_Framework
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the name of the car");
            string carname = Console.ReadLine();
            Console.WriteLine("Enter the colour of the Car");
            string car_color= Console.ReadLine();
            Console.WriteLine("Enter the year car was purchased");
            int yop = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter if Automatic or Manual");
            string gear=Console.ReadLine();
            Console.WriteLine($"Name of Car::{carname} \n Colour of Car::{car_color} \n Year of purchase::{yop} \n The car is::{gear}");
            Console.ReadLine();
        }
    }
}
